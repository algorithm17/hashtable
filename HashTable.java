import java.util.Hashtable;

public class HashTable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		 Hashtable<Integer, String>   hm = new Hashtable<Integer, String>();
		 
	        //put
	        hm.put(1, "Fhang");
	        hm.put(12, "Nat");
	        hm.put(13, "Nick");
	        hm.put(3, "Frame");
	        
	        // Printing
	        System.out.println(hm);
	        
	        //get
	        System.out.println(hm.get(1));
	        
	        //remove
	        hm.remove(1);
	 
	        // Printing
	        System.out.println(hm);

	}

}
