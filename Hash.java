/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Natthakritta
 */
public class Hash<Key, Value> {

    int maxSize = 10;
    Value[] vals = (Value[]) new Object[maxSize];
    Key[] keys = (Key[]) new Object[maxSize];

    private int hash(Key key) {
        return (key.hashCode() & 0x7FFFFFFF) % maxSize;
    }

    public void put(Key key, Value value) {
        int i;
        for (i = hash(key); keys[i] != null; i = (i + 1) % maxSize) {
            if (keys[i].equals(key)) {
                break;
            }
        }
        keys[i] = key;
        vals[i] = value;
    }

    public Value get(Key key) {
        for (int i = hash(key); keys[i] != null; i = (i + 1) % maxSize) {
            if (keys[i].equals(key)) {
                return vals[i];
            }
        }
        return null;
    }

    public Value remove(Key key) {
        for (int i = hash(key); keys[i] != null; i = (i + 1) % maxSize) {
            if (keys[i].equals(key)) {
                keys[i] = null;
                vals[i] = null;
            }
        }
        return null;
    }
}
