/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Natthakritta
 */
public class Main {

    public static void main(String[] args) {
        Hash<String, Integer> hash = new Hash<>();

        //test put
        hash.put("1", 10);
        hash.put("2", 20);
        hash.put("3", 30);
        hash.put("4", 40);

        //test get
        System.out.println("Befor remove");
        System.out.println(hash.get("1"));
        System.out.println(hash.get("2"));
        System.out.println(hash.get("3"));
        System.out.println(hash.get("4"));

        //test remove
        hash.remove("1");
        hash.remove("3");

        //test get
        System.out.println("After remove");
        System.out.println(hash.get("1"));
        System.out.println(hash.get("2"));
        System.out.println(hash.get("3"));
        System.out.println(hash.get("4"));
    }
}
